"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Editorjs tool
 * Feature: Make text underline
 */
class TextUnderline {
    constructor({ api }) {
        //Selected text
        this.selection = null;
        this.inputOpened = false;
        /**
           * Elements
           */
        this.nodes = {
            button: null,
            input: null,
        };
        this.CSS = {
            inlineToolbar: 'ce-inline-toolbar',
            button: 'ce-inline-tool',
            buttonActive: 'ce-inline-tool--active',
            buttonModifier: 'ce-inline-tool--link',
            buttonUnlink: 'ce-inline-tool--unlink',
            inlineToolbarShowed: 'ce-inline-toolbar--showed',
            selectionFlag: 'ce-underline-selection',
        };
        this.toolbar = null;
        this.notifier = null;
        this.commandName = "underline";
        /**
         * Clsoe the inline toolbar
         */
        this.closeToolbar = () => {
            window.getSelection().removeAllRanges();
            this.api.inlineToolbar.close();
            this.api.toolbar.close();
        };
        this.api = api;
        this.notifier = api.notifier;
        this.state = false;
    }
    static get isInline() {
        return true;
    }
    render() {
        this.nodes.button = document.createElement('button');
        this.nodes.button.type = "button";
        this.nodes.button.classList = "ce-inline-tool";
        this.nodes.button.innerHTML = `
           <svg xmlns="http://www.w3.org/2000/svg" height="18" viewBox="0 0 24 24" width="18"><path d="M0 0h24v24H0V0z" fill="none"/><path d="M12 17c3.31 0 6-2.69 6-6V3h-2.5v8c0 1.93-1.57 3.5-3.5 3.5S8.5 12.93 8.5 11V3H6v8c0 3.31 2.69 6 6 6zm-7 2v2h14v-2H5z"/></svg>
        `;
        return this.nodes.button;
    }
    surround(range) {
        if (this.state) {
            return;
        }
        document.execCommand(this.commandName);
    }
    /**
     * Shortcut
     */
    get shortcut() {
        return 'CMD+U';
    }
    checkState(_selection) {
        const isActive = document.queryCommandState(this.commandName);
        this.nodes.button.classList.toggle(this.CSS.buttonActive, isActive);
        return isActive;
    }
    /**
     * Sanitizer rule
     * @return {{span: {class: string}}}
     */
    static get sanitize() {
        return {
            u: {
                class: false // All classes accepted!
            }
        };
    }
}
module.exports = TextUnderline
