# editorjs-text-underline

Editorjs inline tool: Text underline

## Install
* yarn add editorjs-text-underline
* npm i editorjs-text-underline

## Usage
Add a new Tool to the tools property of the Editor.js initial config.

```
const TextUnderline = require("editorjs-text-underline")
var editor = EditorJS({
  ...
  
  tools: {
    ...
    underline: TextUnderline,
  }
  
  ...
});
```

